SaltStack Installation Steps
============================

## Master Server Steps

#### Step 1 - Install SaltStack

* install salt repo
```
yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm
```

* clear expire-cache
```
yum clean expire-cache
```

* install SaltStack
```
yum install -y salt-master salt-ssh
```
 * this installs the following:
  * salt-master
  * salt-minion
    * service not installed until you do a yum install salt-minion
  * salt-cloud
    * service not installed until you do a yum install salt-cloud

#### Open ports in the firewall
* firewalld
```
firewall-cmd --permanent --new-zone=saltstack
```
```
firewall-cmd --reload
```
```
firewall-cmd --permanent --zone=saltstack --add-port=4505-4506/tcp
```
```
firewall-cmd --reload
```
```
firewall-cmd --permanent --zone=saltstack --add-source=[ip address of minion]
```
```
firewall-cmd --reload
```
* iptables
```
iptables -A INPUT -p tcp -s [ip address of minion] --dport 4505:4506 -j ACCEPT
```

# make sure you uncomment the hash_type
```
vim /etc/salt/master
```
hash_type: sha256

#### Show local key
```
salt-call --local key.finger
```

#### List keys
```
[root@web01 salt]# salt-key -L
Accepted Keys:
Denied Keys:
Unaccepted Keys:
minionsvr1
Rejected Keys:
```

#### Accept minion key
```
[root@web01 salt]# salt-key -A
The following keys are going to be accepted:
Unaccepted Keys:
minionsvr1
Proceed? [n/Y] y
```

#### List keys
```
[root@web01 salt]# salt-key -L
Accepted Keys:
minionsvr1
Denied Keys:
Unaccepted Keys:
Rejected Keys:
```  



------------------------------------------

## Minion Server Steps

#### Step 1 - Install SaltStack

* install salt repo
```
yum install https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm
```

* clear expire-cache
```
yum clean expire-cache
```

* install SaltStack
```
yum install -y salt-minion salt-ssh
```
 * this installs the following:
  * salt-master
    * service not installed until you do a yum install salt-master
  * salt-minion
  * salt-cloud
    * service not installed until you do a yum install salt-cloud

# make sure you uncomment the hash_type
```
vim /etc/salt/minion
```
hash_type: sha256



## References

* [Official Installation Steps](https://docs.saltstack.com/en/latest/topics/installation/rhel.html)

* [Official Configuration Steps](https://docs.saltstack.com/en/latest/ref/configuration/index.html)

* [Salt SSH](https://docs.saltstack.com/en/latest/topics/ssh/index.html)
