SaltStack Documentation
=======================

***Master and Minion Installation*** - Steps to install SaltStack on a master and minion server.  

  * [Master and Minion Centos 7 Installation](saltstack-centos7-installation-steps.md)  
  * [Master and Minion Centos 6 Installation](saltstack-centos6-installation-steps.md)  
  * [Master and Minion Centos 5 Installation](saltstack-centos5-installation-steps.md)  

[Master and Minion Configuration](saltstack-configuration-steps.md) - Steps to configure SaltStack on a master and minion server.  

[Troubleshooting](saltstack-troubleshooting.md) - Some troubleshooting notes.  

***SaltStack Examples***  

  * [Minecraft Server Install](examples/minecraft/)  
  * [Squid](examples/squid/)  
