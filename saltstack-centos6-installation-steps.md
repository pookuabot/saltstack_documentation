SaltStack Installation Steps
============================

## Master Server Steps

#### Step 1 - Install SaltStack

* import SaltStack GPG Key
```
rpm --import https://repo.saltstack.com/yum/redhat/6/x86_64/archive/2016.3.4/SALTSTACK-GPG-KEY.pub
```

#### Download SaltStack Centos 6 repo
```
cd /etc/yum.repos.d
wget http://repo.pookuabot.xyz/saltstack/saltstack-centos6.repo
```

* clear expire-cache
```
yum clean expire-cache
```

* install SaltStack
```
yum install -y salt-master salt-ssh salt-api
```
 * this installs the following:
  * salt-master
  * salt-minion
    * service not installed until you do a yum install salt-minion
  * salt-cloud
    * service not installed until you do a yum install salt-cloud

#### Open ports in the firewall
* iptables
```
iptables -A INPUT -p tcp -s [ip address of minion] --dport 4505:4506 -j ACCEPT
```

#### Enable Salt Master on Boot
```
chkconfig salt-master on
```

#### Check Salt Master status
```
service salt-master status
```

#### If it isn't started, make sure you start the service
```
service salt-master start
```

#### Show local key
```
salt-call --local key.finger
```

# make sure you uncomment the hash_type
```
vim /etc/salt/master
```
hash_type: sha256

#### List keys
```
[root@web01 salt]# salt-key -L
Accepted Keys:
Denied Keys:
Unaccepted Keys:
minionsvr1
Rejected Keys:
```

#### Accept minion key
```
[root@web01 salt]# salt-key -A
The following keys are going to be accepted:
Unaccepted Keys:
minionsvr1
Proceed? [n/Y] y
```

#### List keys
```
[root@web01 salt]# salt-key -L
Accepted Keys:
minionsvr1
Denied Keys:
Unaccepted Keys:
Rejected Keys:
```  



------------------------------------------

## Minion Server Steps

#### Step 1 - Install SaltStack

* import SaltStack GPG Key
```
rpm --import https://repo.saltstack.com/yum/redhat/6/x86_64/archive/2016.3.4/SALTSTACK-GPG-KEY.pub
```

#### Download SaltStack Centos 6 repo
```
cd /etc/yum.repos.d
wget http://repo.pookuabot.xyz/saltstack/saltstack-centos6.repo
```

* clear expire-cache
```
yum clean expire-cache
```

* install SaltStack
```
yum install -y salt-minion salt-ssh salt-api
```
 * this installs the following:
  * salt-master
    * service not installed until you do a yum install salt-master
  * salt-ssh
  * salt-api
  * salt-minion
  * salt-cloud
    * service not installed until you do a yum install salt-cloud

# make sure you uncomment the hash_type
```
vim /etc/salt/minion
```
hash_type: sha256



## References

* [Official Installation Steps](https://docs.saltstack.com/en/latest/topics/installation/rhel.html)

* [Official Configuration Steps](https://docs.saltstack.com/en/latest/ref/configuration/index.html)

* [Salt SSH](https://docs.saltstack.com/en/latest/topics/ssh/index.html)
