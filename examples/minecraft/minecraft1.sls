# this will install the Spigot Build tools and build spigot

include:
  - minecraft1

scaffolding spigot directories:
  file.directory:
    - makedirs: yes
    - names:
      - /opt/minecraft/mc_servers/minecraft1
    - mode: 755

install buildtools:
  file.managed:
    - name: /opt/minecraft/mc_servers/minecraft1/BuildTools.jar
    - source: https://hub.spigotmc.org/jenkins/job/BuildTools/lastStableBuild/artifact/target/BuildTools.jar
    - source_hash: md5=fc7376ac47ccd57dbd17e141e71389b6
    - mode: 755
    - require:
      - file: /opt/minecraft/mc_servers/minecraft1

build spigotmc:
  cmd.wait:
    - name: java -jar BuildTools.jar
    - cwd: /opt/minecraft/mc_servers/minecraft1
    - watch:
      - file: /opt/minecraft/mc_servers/minecraft1/BuildTools.jar

deploy spigotmc:
  cmd.run:
    - use:
      - cmd: java -jar BuildTools.jar
    - name: mv spigot*.jar /opt/minecraft/mc_servers/minecraft1/spigot.jar
    - onlyif: ls spigot*.jar
    - watch:
      - cmd: java -jar BuildTools.jar
    - require:
      - file: /opt/minecraft/mc_servers/minecraft1/BuildTools.jar
