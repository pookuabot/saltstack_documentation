include:
  - squid-setup

/etc/squid:
  file.managed:
    - source: salt://files/squid.conf
    - mode: 644
{% if grains['os'] == 'Centos' %}
     context:
        custom_var: "override"
{% endif %}

/etc/sysconfig/network-scripts/ifcfg-br0:
  file.managed:
    - source: salt://files/network/ifcfg-br0
    - mode: 644
{% if grains['os'] == 'Centos' %}
     context:
        custom_var: "override"
{% endif %}

/etc/sysconfig/network-scripts/ifcfg-enp1s0:
  file.managed:
    - source: salt://files/network/ifcfg-enp1s0
    - mode: 644
{% if grains['os'] == 'Centos' %}
     context:
        custom_var: "override"
{% endif %}

/etc/sysconfig/network-scripts/ifcfg-enp3s0:
  file.managed:
    - source: salt://files/network/ifcfg-enp3s0
    - mode: 644
{% if grains['os'] == 'Centos' %}
     context:
        custom_var: "override"
{% endif %}

# Will be adding restart network
